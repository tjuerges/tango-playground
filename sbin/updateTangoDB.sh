#!/usr/bin/env bash

[[ ${#} -lt 3 ]] && { echo -e "\nYou must specify either -u for updating or -l for replacing the TangoDB.\nYou also need to provide a JSON file for the TanfoDB."; exit 1; }

update=0
load=0
while getopts "luf:" opt; do
    case ${opt} in
        f)
            file=${OPTARG}
            ;;
        l)
            [[ ${update} -ne 0 ]] && { echo "You cannot update and replace the TangoDB at the same time. Choose either the -u or the -l option."; exit -1; }
            let load=1
            ;;
        u)
            [[ ${load} -ne 0 ]] && { echo "You cannot update and replace the TangoDB at the same time. Choose either the -u or the -l option."; exit -1; }
            let update=1
            ;;
    esac
done

[[ ${update} -eq 0 ]] && [[ ${load} -eq 0 ]] && { echo -e "\nYou must specify either -u for updating or -l for replacing the TangoDB."; exit -1; }

# Copy the file into the container. Tango's dsconfig will then read it from
# the target location in the container.
${CONTAINER_ENGINE} cp "${file}" "${CONTAINER_NAME_PREFIX}"dsconfig:/tmp/dsconfig-update-TangoDB.json || exit 1

# Now ask dsconfig to load/update the settings.
# Do not change -i into -it as this would break integration tests in Gitlab ci!
update_option="--update"
[[ ${update} -eq 1 ]]  && [[ ${load} -eq 1 ]] && update_option=""
${CONTAINER_ENGINE} exec -i "${CONTAINER_NAME_PREFIX}"dsconfig json2tango --write ${update_option} /tmp/dsconfig-update-TangoDB.json

# Somehow json2tango does not return 0 on success
exit 0
