#! /usr/bin/env bash

# Copy a JSON dump of the TangoDB to stdout.
# Do not change -i to -it because it is incompatible with gitlab ci!
${CONTAINER_ENGINE} exec -i "${CONTAINER_NAME_PREFIX}"dsconfig python -m dsconfig.dump
