"""This is a very simple Tango Device"""

from tango import DevState, GreenMode, DevDouble, DevVoid
from tango.server import Device, command, attribute, run

import numpy

from time import sleep
from datetime import datetime

# I want to print neat ISO timestamps.
def now():
    return datetime.utcnow().strftime('%FT%T.%f')[:-3]

class SimpleDevice(Device):
    def init_device(self):
        super().init_device()
        self._sleepTime = 0.0
        self.set_state(DevState.ON)

    @command(dtype_in = DevDouble, dtype_out = DevDouble)
    def wait_cmd(self, sleepTime: DevDouble = None) -> DevDouble:
        print(f'{now()} The command will wait for {sleepTime}s before returning...')
        sleep(sleepTime)
        print(f'{now()} The command waited for {sleepTime}s.')
        return sleepTime

    @attribute
    def sleepTime(self) -> None:
        return self._sleepTime

    @sleepTime.write
    def sleepTime(self, sleepTime: DevDouble = None) -> DevVoid:
        print(f'{now()} Setting the sleep time.')
        self._sleepTime = sleepTime

    @attribute
    def test_attribute(self) -> DevDouble:
        print(f'{now()} Wait for {self._sleepTime}s before returning the attribute value...')
        sleep(self._sleepTime)
        self._test_attribute = numpy.random.random()
        print(f'{now()} Waiting for {self._sleepTime}s done. Will now return the attribute value.')
        return self._test_attribute

if __name__ == '__main__':
    run((SimpleDevice, ))
    print(f'{now()}')
