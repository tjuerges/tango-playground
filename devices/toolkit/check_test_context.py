def test_read_attribute_and_count_calls():
    from tango.server import Device, attribute
    from tango.test_context import DeviceTestContext
    from tango.test_utils import assert_close
    class TestDevice(Device):
        _voltage = 0.0
        _voltage_read_count = 0

        @attribute(dtype=float)
        def voltage(self):
            self._voltage_read_count += 1
            return self._voltage

        @attribute(dtype=int)
        def num_voltage_reads(self):
            return self._voltage_read_count

    with DeviceTestContext(TestDevice) as proxy:
        # high-level API read
        assert proxy.num_voltage_reads == 0
        high_level_api_read_value = proxy.voltage
        assert proxy.num_voltage_reads == 1
        assert_close(high_level_api_read_value, 0.0)

        # low-level API read
        low_level_api_reading = proxy.read_attribute("voltage")
        assert proxy.num_voltage_reads == 2
        assert_close(low_level_api_reading.value, 0.0)
