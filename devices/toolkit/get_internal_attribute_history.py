#! /usr/bin/env python3

from tango import DeviceProxy
from numpy import array, transpose

def get_internal_attribute_history(device: DeviceProxy, attribute_name: str, depth: int = 10):
    try:
        history = array(device.attribute_history(attr_name = attribute_name, depth = depth))
    except Exception as e:
        raise ValueError(f"Cannot access the internal history of the attribute '{device.name()}/{attribute_name}") from e

    history_values = array([entry.value for entry in history])
    values = history_values.transpose()
    return values
