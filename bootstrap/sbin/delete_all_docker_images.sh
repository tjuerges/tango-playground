#! /usr/bin/env bash

function help()
{
    echo "You need to add a parameter \"YES_DELETE_ALL\" in order to really remove all container images."
    exit 0
}

if [ ${#} -ne 1 ]; then
    help
elif [ "${1}" != "YES_DELETE_ALL" ]; then
    help
fi

read -p "If you certain that you want to delete all container images, then enter now \"YES_DO_IT\" " reply
if [ "${reply}" != "YES_DO_IT" ]; then
    echo "You really need to enter \"YES_DO_IT\" in order to confirm."
    exit 0
else
    images="$(for i in $(podman images | egrep -v "REPOSITORY" | awk '{printf "%s:%s\n", $1, $2}'); do printf "%s " ${i}; done)"
    if [ ${#images} -eq 0 ]; then
        echo "There are no images to delete."
    else
        echo -e "Will now delete the following Podman images:"
        for image in ${images}; do
            printf "\t%s\n" "${image}"
        done
        ${CONTAINER_ENGINE} rmi ${images}
    fi
fi

