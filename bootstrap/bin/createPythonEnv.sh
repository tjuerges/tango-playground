#! /usr/bin/env bash

# Convenience script:
# Create a Python3 venv for the user in ${PLAYGROUND_DIR}/env. This script
# will not overwrite an existing venv.

# Check if we are already in a venv. Do not continue if we are.
[[ -z ${VIRTUAL_ENV} ]] || { echo -e "\nThis is currently a Python3 venv.\nWill not create a new one which could possibly overwrite this one.\n"; exit -1; }

# Check if ${PLAYGROUND_DIR} is set. If not, we do not know where the venv directory
# should be. Do not continue if it is not set.
[[ -z ${PLAYGROUND_DIR} ]] && { echo -e "\nThe environment variable PLAYGROUND_DIR is not set.\nYou may want to source bootstrap/etc/playgroundrc.sh!\n"; exit -2; }

# Check if the directory already exists where we want to create the venv.
# Do not continue if it exists.
[[ -d ${PLAYGROUND_DIR}/env ]] && { echo -e "\nThe Python3 venv for the Tango playground exists already. Please remove it first!\n"; exit -3; }

# Now go ahead and create the venv in ${PLAYGROUND_DIR}/env.
echo -e "\nWill create a Python3 venv in \"${PLAYGROUND_DIR}/env\".\n"
python3 -m venv --upgrade-deps ${PLAYGROUND_DIR}/env

# Check if the user wants the default packages installed in the venv.
read -n 1 -p "Do you want to install the packages in \${PLAYGROUND_DIR}/bootstrap/etc/requirements.txt? Press Y to install them, any other key skips the installation: " YESNO

echo

# Activate the venv if Y was pressed.
[[ "${YESNO}" = "Y" ]] && { . ${PLAYGROUND_DIR}/env/bin/activate; python3 -m pip install -r ${PLAYGROUND_DIR}/bootstrap/etc/requirements.txt; deactivate; }

echo -e "\nDone.\nPlease do not forget to activate your new venv with\n\n\t. \${PLAYGROUND_DIR}/env/bin/activate\n\n"
