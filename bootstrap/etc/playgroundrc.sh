#! /usr/bin/env bash -e
# Set up the Tango playground environment.

if [ "$(uname -s)" = "Darwin" ]; then
    # Set the number of parallel build jobs to a sensible default.
    export COMPOSE_BUILD_JOBS=$(sysctl -n hw.logicalcpu)
    # Make certain that this system has the realpath command available.
    which -s realpath
    [[ ${?} -ne 0 ]] && ( echo -e "\nERROR: The command \"realpath\" could not be founnd.\nSorry, but you need to install brew's coreutils package:\n\tbrew install coreutils\n"; exit -1; )
else
    # Same for other OSes
    export COMPOSE_BUILD_JOBS=$(nproc)
    which realpath 2&>1 >/dev/null
    [[ ${?} -ne 1 ]] && ( echo -e "\nERROR: The command \"realpath\" could not be found.\nSorry, but you need to install a package which provides the realpath command.\n"; exit -2; )
fi

# Pass a directory as first parameter to this script.  This will
# then be used as PLAYGROUND_DIR.  Otherwise this file's directory
# be used to determine the tango directory location.
ABSOLUTE_PATH=$(realpath $(dirname ${BASH_SOURCE}))
export PLAYGROUND_DIR=${1:-$(realpath ${ABSOLUTE_PATH}/../..)}

# This export defines the name of the playground's VM for macOS. This
# is used in the Makefile and here in the next export.
export PLAYGROUND_VM_NAME=tango-playground

# Set here which container engine you want to use.
# It is either podman or docker.
# Note that this setting here does not overwrite the value of an already
# exported variable CONTAINER_ENGINE.
export CONTAINER_ENGINE=${CONTAINER_ENGINE:-docker}
#export CONTAINER_ENGINE=${CONTAINER_ENGINE:-podman)

# This export makes certain that podman and docker can co-exist.
[[ "${CONTAINER_ENGINE}" == "podman" ]] && export DOCKER_HOST="unix://${HOME}/.local/share/containers/podman/machine/${PLAYGROUND_VM_NAME}/podman.sock"

# This allows mounting of ${PLAYGROUND_DIR} as /opt/playground in the containers.
export TANGO_PLAYGROUND_LOCAL_DIR=${TANGO_PLAYGROUND_LOCAL_DIR:=${PLAYGROUND_DIR}}/
export TANGO_PLAYGROUND_CONTAINER_DIR=${TANGO_PLAYGROUND_CONTAINER_DIR:=/opt/playground}/
export TANGO_PLAYGROUND_CONTAINER_MOUNT=${TANGO_PLAYGROUND_LOCAL_DIR}:${TANGO_PLAYGROUND_CONTAINER_DIR}:rw

# This will allow to mount ${PLAYGROUND_DIR}/container-scratch as /home/tango:rw.
# iTango, Pogo and Jive like to store their stuff there and this
# will keep those files around even after a big docker purge.
#
# If you do not like the default, just change ${PLAYGROUND_DIR}/container-scratch to
# something that matches better what you need.
export CONTAINER_SCRATCH_DIR=${CONTAINER_SCRATCH_DIR:=${PLAYGROUND_DIR}/container-scratch}/
export CONTAINER_SCRATCH=${CONTAINER_SCRATCH_DIR}:/home/tango:rw

# If you feel adventurous, you can set the hostname to something unusual.
# Recommended for instance if you are running Linux on Windows, a.g.a WSL.
# You can do this here but be aware that this host name must be resolve to
# this host.
# If unsure, leave the defaults as they are.
if [ -f /run/WSL ]; then
    export HOSTNAME=${HOSTNAME:=host.podman.internal}
else
    export HOSTNAME=${HOSTNAME:=$(hostname -f)}
fi

# In case you run multiple Podman networks on the same host in parallel, you
# need to specify a unique network name for each of them.
# This is the default but you can overwrite it.
export NETWORK_MODE=${NETWORK_MODE:=tangonet}

# It is assumed that the Tango host, the container that runs the
# TangoDB, is databaseds.
# If this is not true, then modify to the Tango host's FQDN and port.
# Example:  export TANGO_HOST=station-xk25.astron.nl:10000
export TANGO_HOST=${TANGO_HOST:=databaseds:10000}

# Host name to which to send our container logs. Needs to be resolvable from
# the host.
export LOG_HOSTNAME=${LOG_HOSTNAME:=localhost}

# Add the paths of this Tango playground for convenience.
# Feel free to comment this out if it bothers you.
export PATH="${PATH}:${PLAYGROUND_DIR}/bin:${PLAYGROUND_DIR}/sbin:${PLAYGROUND_DIR}/bootstrap/bin:${PLAYGROUND_DIR}/bootstrap/sbin"

#
# CI_BUILD_ID does not exist. See
# https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
if [ ! -z ${CI_BUILD_ID+x} ]; then
    export CONTAINER_NAME_PREFIX=${CI_BUILD_ID}-
elif [ ! -z ${CI_JOB_ID+x} ]; then
    export CONTAINER_NAME_PREFIX=${CI_JOB_ID}-
else
    unset CONTAINER_NAME_PREFIX
fi

#
# NO MODIFICATION BEYOND THIS POINT!
#

# On Linux systems the .Xauthority does sometimes not exist. I do not
# understand why that is. Just create it.
if [ $(uname -s) = Linux ]; then
    touch ~/.Xauthority
fi
# Allow read access for everybody to allow podman the forwarding of X11.
[[ -f ~/.Xauthority ]] && chmod a+r ~/.Xauthority

# Source the PLAYGROUND Python3 venv if it exists.
[ -z ${VIRTUAL_ENV} ] && [ -d ${PLAYGROUND_DIR}/env ] && source ${PLAYGROUND_DIR}/env/bin/activate
