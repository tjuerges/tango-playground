#! /usr/bin/env bash

# If parameters are passed, just pass them on to the iTango container.
parameters="itango3"
[[ ${#} -ge 1 ]] && parameters=${@}
${CONTAINER_ENGINE} exec -it "${CONTAINER_NAME_PREFIX}"itango ${parameters}
