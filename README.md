# Tango Controls Playground

[TOC]

## Introduction

This repository is supposed to be a simple and easy way to toy around with Tango Controls devices in a fully functional Tango Controls set up.

This set up contains:

- A Tango DB which can easily be configured. - And messed up! ;-)
- Jive, the Swiss Army Knife for configuring small Tango Controls systems.
- A primitive archiver that does what is expected if configured right: Storing attribute values.
- iTango, the ultimate Tango Controls console.

# Installation

## Prerequisites

You will need:

* `bash`
    This one is really important, so do not skip it. Bash is needed for every script, the RC file and some config stuff in the `Makefile`. It is possible that other shells also work, but there is no guarantee, because here only Bash is supported. Install it with your favourite package manager if it is not already installed.

    * :point_up: **Note macOS:** Make certain that your terminal session runs already Bash! The whole playground either does not work well or does not work at all from zsh.

* `make`: The top-level `Makefile` needs a tool that knows what to do with it and this tool is `make`. On macOS this comes preinstalled with the OS, on Linux you might need to install it from your distribution. Check the output of `which make` to see if you need to install it.

* You can choose between `docker` or `podman` as the container engine.

    * `podman`:

        * macOS: Install `podman` with brew.
            ```bash
            brew install podman
            ```

            * :point_up: **Note:** After the `podman` installation, `podman` advises to run the `podman-mac-helper` . Do **not** run it:exclamation: It is not needed and it would interfere with an existing Docker Desktop CE installation. Where needed, the `DOCKER_HOST`environment variable is set instead.

        * Linux: There will likely be packages for your distribution or package manager.

    * `docker`:

        * macOS: Install Docker Desktop.
        * Linux: Use your distro's packages or install Docker Desktop.


* `docker-compose`: If you opt to use `podman` you need to install this yourself.

    - macOS: You have three options.

        - Use the Makefile's convenient `bootstrap` target. See [below](#The easy way: All in one).

        - Install `docker-compose` with brew.

            ```shell
            brew install docker-compose
            ```

        - Install `docker-compose` with pip3 in your favourite venv. You could make use of the `make env` command. See here.

            ```shell
            python3 -m pip install docker-compose
            ```

        :point_up: **Note:** Make certain that you really use either the `brew` or the `pypi` version of `docker-compose`. The version provided by Docker Desktop CE is incompatible with podman.

    - Linux: Install your distribnution's package with your favourite package manager.

* macOS specific packages:

    *  `realpath` from brew's `coreutils` package. Install it with brew:

        ```shell
        brew install coreutils
        ```


That's it. :handshake: You do not need anything else.

## Getting this repo:

If you do not have provided Gitlab with your public SSH key, then cloning the repo via https will be your route to success:

```shell
git clone https://gitlab.com/tjuerges/tango-playground.git
```

In case you have already a public SSH key stored at Gitlab's, then you can clone the repo via ssh:

```bash
git clone git@gitlab.com:tjuerges/tango-playground.git
```

This should do the trick and you are now ready to go.

# Got the repo. And now?

## What is all that?

The repository contains various sorts of files:

- Shell scripts for admin stuff that is occationally needed.
- Top level `Makefile` to run everything.
- Docker files to build a couple of containers.
- `docker-compose` files to make things happen.
- JSON files for the Tango database (Tango DB).
- RC file to set-up necessary environment variables. Always source this file first!
- Python files that are examples for Tango Device Servers and a couple of helper modules.
- Files that I forgot to mention here. ;-)

## Is there a pattern in this mess?

One could group all of these files like this:

- Groundskeeper stuff: Essential to get started and maintain the playground.
    - Top level `Makefile` ( This is the infrastructure's brain: pull, build, start and stop containers)
    - Everything in `bootstrap` (RC file for providing essential information to the top level `Makefile`, erasing all of the Podman images, etc.)
    - Files and directories in `podman` (Everything that is needed to pull, build and run the containers)
    - Scripts in `sbin` (dump, load or modify the TangoDB)
- Heart and soul stuff: Essential to run devices
    - Scripts in `bin`
        - `start-ds.sh PATH_TO_PYTHON_DEVICE_SERVER_PY_FILE LOCATION_AS_DEFINED_IN_TANGODB`: This will run your Tango Controls device (Python3!) in the `itango` container.
            :point_up: **Note:** The Python device server file needs to be readable from within the iTango container. Your `${HOME}` directory is made available as `/hosthome` in the containers. If you put the Python device server file in a directory within your `${HOME}`, then there should not be a problem. I am aware that this is not too elegant, but it is the quickest way to make availabe in the containers what one is working on.
        - `itango.sh`: This starts an iTango console and allows you to access your devices.
    - Python files in `devices`: A couple of example devices.You want to check that the TangoDB contains those devices, otherwise you will not be able to start 'em. See below.
- Essential to modify/update the system
    - Scripts in `sbin`
    - JSON files for TangoDB in `TangoDB` and in subdirectories of `devices`. Usually they can be found there also in a `TangoDB` subdirectory. These can be loaded into the `TangoDB` with the provided `updateTangoDB` script.

### Helper scripts

There are a handful of helper scripts, that are scattered around in the directories `bin`, `sbin`, `bootstrap/bin` and  `boostrap/sbin`. These exist to simplify the use of this containerised Tango Controls system. Namely `updateTangoDB.sh`, `itango.sh` and `start-ds.sh`.

## Source the RC file, Luke! Source the RC file!

It is strongly advised to **always** source the file `bootstrap/etc/playgroundrc.sh`. This will export a handful of environment variables which will be used by the central `Makefile` in the top level directory and also by the helper scripts.

```
. tango-playground/bootstrap/etc/playgroundrc.sh
```

The RC file will also activate an optional Python virtual environment, which will be expected at the repo's top level directory and there in  `env`. If there is no virtual envinronment directory, then that is also prefectly fine. The virtual environment or the lack of it will not affect the functioning of the Tango Controls containers at all, because it is not used from within the Tango Controls containers. It would just be there for your convenience. For instance if you decide to control your Tango Controls Podman containers from Python3

:point_up: **Note**: Make certain that you choose either `podman` or `docker` as your container engine. This can be easily done by exporting an environment variable or by setting it in `playgroundrc.sh`.

- Export an environment variable `CONTAINER_ENGINE` before you source `playgroundrc.sh`:
    ```bash
    export CONTAINER_ENGINE=podman
    . tango-playground/bootstrap/etc/playgroundrc.sh
    ```

- Or edit the setting in `playgroundrc.sh`:
    ```bash
    # Set here which container engine you want to use.
    # It is either podman or docker.
    # Note that this setting here does not overwrite the value of an already
    # exported variable CONTAINER_ENGINE.
    #export CONTAINER_ENGINE=${CONTAINER_ENGINE:-podman)
    export CONTAINER_ENGINE=${CONTAINER_ENGINE:-docker}
    
    ```

    But note that setting this in `playgroundrc.sh` will modify the file and hence git will notice it. If you ask me, I prefer exporting `CONTAINER_ENGINE` before I source `playground.sh`.

## PLAYGROUND_DIR

The most important environment variable that the RC file sets is `${PLAYGROUND_DIR}`. It will be set to the top level directory of the repository. The top level `Makefile` (see below) and all helper scripts (next chapter) will make heavy use of this environment variable.

If something does not work, verify that the environment variable `${PLAYGROUND_DIR}` is set:

```shell
printenv ${PLAYGROUND_DIR}
```

##  Top level Makefile

The Makefile in the root directory serves a couple of purposes.

- Print the help: `make help`
- Create a Python3 venv in `${PLAYGROUND_DIR}/env`: `make venv`
- Pull images and build the playground in one go: `make bootstrap`
- Pull images: `make pull`
- Build images that include some extras: `make build`
- Start a minimal system: `make minimal`
- Start individual containers: `make start itango`
- Stop individual containers: `make stop itango`
- Stop the entire system: `make stop`
- Clean up everything. That is images, volumes, the network config and the podman VM: `make clean`
- And some other things that I forgot to mention here. `make help` is your friend.

If you cannot make it work, verify that you have sourced the RC file:

```shell
printenv PLAYGROUND_DIR
/Users/thomas/workspace.thomas/tango-playground
```

 If the output is empty, source the RC file `bootstrap/etc/playgroundrc.sh`!

## [Optional] Fancy: Create your Python virtual environment

One of the helper scripts is `createPythonEnv.sh`. It will setup a Python virtual environment for you in `${PLAYGROUND_DIR}/env`. The `Makefile` has also a target `venv` that will run this script for you.

Note that this is an optional step and it is not mandatory. The existence of this virtual environment will not affect the Tango Controls containers in any way.

While the script runs, it will ask you if you would like to install some packages from the bootstrap/etc/requirements.txt file. I have added `docker-compose` to the file for your convenience. You can either follow the recommendations that the script provides or not. This is entirely up to you.

**Note:** As emphasised above: Whatever gets installed in this virtual environment will not be available to any of the Tango Controls containers.

```shell
createPythonEnv.sh

Will create a Python3 venv in "/Users/thomas/workspace.thomas/thomas/tango-playground/env".

Do you want to install the packages in ${PLAYGROUND_DIR}/bootstrap/etc/requirements.txt? Press Y to install them, any other key skips the installation: Y
Collecting astropy
  Using cached astropy-5.0.1-cp39-cp39-macosx_10_9_x86_64.whl (6.9 MB)
[...]

Done.
Please do not forget to activate your new venv with

	. ${PLAYGROUND_DIR}/env/bin/activate
```

# Good, got here. Now: Bootstrap

You need to bootstrap the entire enchilada at least once. After that it is just keeping the images and your Tango DB up to date.

## The easy way: All in one

Just source the RC file

```bash
. bootstrap/etc/playgroundrc.sh
```

and run

```bash
cd ${PLAYGROUND_DIR}
make bootstrap
```

That's it. This will take a moment though. Perhaps you would like to sit down and check your e-mails while the system is creating the podman VM, starting it up, then pulling all the various Podman images and building the custom ones. Once the bootstrapping is finished, you can skip to the chapter where we start the machinery.

Or you are more interested in how things are done. Then dive into what `make bootstrap` actually does. This is described in the next chapter.

## The hard way: Manual work

You have sourced the RC file, haven't you?

```shell
. bootstrap/etc/playgroundrc.sh
```

If you just came over from the previous chapter where you already ran `make bootstrap`, then you are good. No need to do anything of this here, because this is in detail what `make bootstrap` does. You can fetch a drink of your preference, take a break and then skip to the next chapter where we start the machinery.

You are still here. This likely means that you have not just run `make bootstrap` or you are genuinely interested in what happens when one runs `make bootstrap`.

OK. We skip the ugly stuff where podman gets bootstrapped (see the `podman_*` targets in the `Makefile`) and begin with pulling of the images that are needed for this playground:

```shell
cd ${PLAYGROUND_DIR}
make pull
```

This will make certain that podman's VM is started and then download the images that are needed to get this thing going. Unless you have a 1TB/s connection then this is now a good time to get a cup of tea, because this is going to take a moment or two, depending in your network bandwidth and your computer's prowess.

## Build the customised images

### iTango

After the base images have been downloaded, the customised iTango image can be built. You might wonder why that is necessary, because there is a prefectly fine SKAO iTango image available? The simple reason is: While we at SKAO try to keep the images as light as possible, I like some additional tools for debugging when I am tinkering with Tango Controls.

Right, let's get cracking:

```shell
make build
```

If there are no error messages, then you are now ready to go. If there are error messages though, then you just found an excuse to spend some quality time with podman, Makefiles and your computer over the weekend. :grin: Or you just admit that the simple bootstrapping would have been easier, head over to the repo page, [submit an issue with the error message](https://gitlab.com/tjuerges/tango-playground/-/issues/new), make it my problem and retry from [here](#The easy way: All in one). :wink:

### hdbpp-configurator

This step will also build the `hdbpp-configurator` image on top of the `Jive`image. The `hdbpp-configurator`can help you setting up archiving of attributes.

## Creating the scratch directory

In order to preserve settings and configurations of the tools that run in containers, the playground maps the directory `${PLAYGROUND_DIR}/container-scratch` to `/home/tango` in the containers. The directory can be manually be created:

```shell
make scratch
```

The `bootstrap` target also takes care of it. Note that an existing scratch directory will not be overwritten or its contents deleted.

If you do not like the default location, you can set it in `${PLAYGROUND_DIR/bootstrap/etc/playgroundrc.sh}`. Search there for `${CONTAINER_SCRATCH_DIR}` and modify it to the path that you prefer.

**Note:** The path of the scratch directory must be a directory under your `${HOME}` directory! This is a limitation that comes from how podman's VM works on macOS. Sorry!

# Time to start it up

A minimal Tango system consist of three containers:

- tangodb as TangoDB's SQL database.
- databaseds as the TangoDB device server.
- dsconfig to be able to modify the TangoDB.

And this is how it can be started:

```shell
make minimal
# Only execute this if podman is the selected container engine.
# After creation, the VM needs to be restarted because sometimes the
# podman client fails to connect.
if [ $(podman machine list --noheading --format "{{.Name}}" | grep -c tango-playground) -eq 0 ]; then podman machine init --rootful --image-path stable --cpus 4 --disk-size 60 --memory 4096 --volume /Users/thomas:/Users/thomas tango-playground; podman machine stop tango-playground; sleep 10; podman machine start tango-playground; fi
# Only execute this if podman is the selected container engine.
if [ $(podman machine list --noheading --format "{{.Name}}:{{.LastUp}}" | grep -c "tango-playground:Currently running") -eq 0 ]; then podman machine start tango-playground; fi
Starting machine "tango-playground"
Waiting for VM ...
Mounting volume... /Users/thomas:/Users/thomas
API forwarding listening on: /Users/thomas/.local/share/containers/podman/machine/tango-playground/podman.sock

The system helper service is not installed; the default Docker API socket
address can't be used by podman. If you would like to install it run the
following commands:

	sudo /usr/local/Cellar/podman/4.1.0/bin/podman-mac-helper install
	podman machine stop tango-playground; podman machine start tango-playground

You can still connect Docker API clients by setting DOCKER_HOST using the
following command in your terminal session:

	export DOCKER_HOST='unix:///Users/thomas/.local/share/containers/podman/machine/tango-playground/podman.sock'

Machine "tango-playground" started successfully
# Only execute this if podman is the selected container engine.
podman network inspect tangonet &> /dev/null || ([ ${?} -ne 0 ] && podman network create tangonet)
tangonet
podman network inspect 9000-tangonet &> /dev/null || ([ ${?} -ne 0 ] && podman network create 9000-tangonet -o mtu=9000)
9000-tangonet
DISPLAY=192.168.222.136:0 CONTAINER_SCRATCH=/Users/thomas/workspace.thomas/tango/tango-playground/container-scratch/:/home/tango:rw XAUTHORITY=/hosthome/.Xauthority TANGO_HOST=databaseds:10000 MYSQL_HOST=tangodb:3306 HOSTHOME=/Users/thomas:/hosthome:rw HOSTNAME=okeanos.senmut.net LOG_HOSTNAME=localhost NETWORK_MODE=tangonet XAUTHORITY_MOUNT=/Users/thomas/.Xauthority:/hosthome/.Xauthority:ro CONTAINER_NAME_PREFIX= COMPOSE_IGNORE_ORPHANS=true CONTAINER_EXECUTION_UID=502 CONTAINER_EXECUTION_GID=20 DOCKER_HOST=unix:///Users/thomas/.local/share/containers/podman/machine/tango-playground/podman.sock docker-compose -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/tango.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/networks.yml up -d
WARNING: Some networks were defined but are not used by any service: data
Creating volume "containers_tangodb" with default driver
Pulling tangodb (artefact.skao.int/ska-tango-images-tango-db:10.4.16)...
[...]
5202a3191b7d: Download complete
Pulling databaseds (artefact.skao.int/ska-tango-images-tango-cpp:9.3.11)...
a99022680c28: Download complete
[...]
b45d3914e3c6: Download complete
Pulling dsconfig (artefact.skao.int/ska-tango-images-tango-dsconfig:1.5.5)...
53b9713cc6cd: Already exists
[...]
a88fed2f1df3: Already exists
3326c8cd9ac9: Download complete
[...]
b6210104945b: Download complete
Creating tangodb ... done
Creating databaseds ... done
Creating dsconfig   ... done
```

You can verify that everything is in order with `make status`:

```shell
make status
DISPLAY=192.168.222.136:0 CONTAINER_SCRATCH=/Users/thomas/workspace.thomas/tango/tango-playground/container-scratch/:/home/tango:rw XAUTHORITY=/hosthome/.Xauthority TANGO_HOST=databaseds:10000 MYSQL_HOST=tangodb:3306 HOSTHOME=/Users/thomas:/hosthome:rw HOSTNAME=okeanos.senmut.net LOG_HOSTNAME=localhost NETWORK_MODE=tangonet XAUTHORITY_MOUNT=/Users/thomas/.Xauthority:/hosthome/.Xauthority:ro CONTAINER_NAME_PREFIX= COMPOSE_IGNORE_ORPHANS=true CONTAINER_EXECUTION_UID=502 CONTAINER_EXECUTION_GID=20 DOCKER_HOST=unix:///Users/thomas/.local/share/containers/podman/machine/tango-playground/podman.sock docker-compose --env-file /Users/thomas/workspace.thomas/tango/tango-playground/containers/.env -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/archiver.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/hdbpp-configurator.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/itango.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/jive.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/networks.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/pogo.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/tango.yml ps
WARNING: Some networks were defined but are not used by any service: data
   Name                 Command               State         Ports
-----------------------------------------------------------------------
databaseds   /usr/local/bin/wait-for-it ...   Up ()   :10000->10000/tcp
dsconfig     sh -c wait-for-it.sh datab ...   Up ()
tangodb      docker-entrypoint.sh mariadbd    Up ()   :3306->3306/tcp
```

# Running stuff

Here is now how you can start device servers (Python3) and access devices.

## Adding a device server to TangoDB

As an example, I walk you through how to add the RandomData device, run its device server and access it in `iTango`. We begin with adding the device server and the device instances to TangoDB. The helper script `updateTangoDB.sh` comes here very handy:
```shell
updateTangoDB.sh -u -f ${PLAYGROUND_DIR}/devices/randomData/TangoDB/RandomData.json
```
The `-u` parameter instructs `updateTangoDB.sh`to update and not replace the existing TangoDB. The `-f` parameter is for the JSON file. That's all. Nothing more. Done.

## Running the device server

Now it is time to fire up the `iTango` container:
```shell
cd ${PLAYGROUND_DIR}
make start itango
```
Once the iTango container has started - verify with `make status`! -, you can run the device server:
```shell
start-ds.sh ${PLAYGROUND_DIR}/devices/randomData/RandomData.py tango-playground
```
That's all there is to it and it is really that simple!

The first parameter is the full path to your device server in Python3 and the second parameter is the location as spacified in the TangoDB.

**Note:** This example assumes that your `${PLAYGROUND_DIR}` is a directory in your `${HOME}` directory. This is a requirement for the flexibility of the `start-ds.sh` helper script. The technical reason is that your `${HOME}` directory is made available as `/hosthome` in the containers and the `start-ds.sh` script assumes that the device server file can be located in `/hosthome`.

## Accessing a Tango device

It would not be much fun if the devices could not be accessed. So let's try that now. Start an `iTango` console:
```shell
itango.sh
ITango 9.3.3 -- An interactive Tango client.

Running on top of Python 3.7.3, IPython 7.21 and PyTango 9.3.3

help      -> ITango's help system.
object?   -> Details about 'object'. ?object also works, ?? prints more.

IPython profile: tango

hint: Try typing: mydev = Device("<tab>

In [1]:
```
Now create a device proxy for the `tango-playground/RandomData/1` device and fetch its attribute list:
```python
In [1]: dp = DeviceProxy('tango-playground/RandomData/1')

In [2]: dp.get_attribute_list()
Out[2]: ['rnd1', 'rnd2', 'rnd3', 'rnd4', 'rnd5', 'rnd6', 'rnd7', 'rnd8', 'rnd9', 'rnd10', 'rnd11', 'rnd12', 'rnd13', 'rnd14', 'rnd15', 'rnd16', 'rnd17', 'rnd18', 'rnd19', 'rnd20', 'State', 'Status']
```

Finally read some attribute values:
```python
In [4]: dp.rnd7, dp.rnd13
Out[4]: (0.20513967707709146, 0.1562026222108246)
```
That's easy, isn't it?
# Maintenance

## Working with the the TangoDB

It is easy to dump, restore or modify the TangoDB from the command line. A couple of scripts are provided that will make your life much easier.

### Exporting the TangoDB as JSON

```shell
dumpTangoDB.sh
```

dumpTangoDB.sh will copy the content of the entire TangoDB as JSON to stdout. If you would like to store the contents in a file, just redirect the output like so:

```shell
dumpTangoDB.sh > foo.json
```

### Importing JSON into TangoDB

If you ever need to replace the entire TangoDB or just want to merge some modifications, then the `updateTangoDB.sh` script is your friend.

#### Replacing TangoDB

Replacing the entire TangoDB with your JSON file is easy:

```shell
updateTangoDB.sh -l -f foo.json
```

Note that executing the script with the `-l` parameter will really **replace** the entire TangoDB with the contents of the JSON file. You have been warned!

#### Updating TangoDB

A less dangerous way to modify the TangoDB is to just **update** or **add** modifications. This can be done with the same script but passing the `-u` parameter instead of `-l`:

```shell
updateTangoDB.sh -u -f foo.json
```

You will find a couple of JSON files in the TangoDB directories in the repo. Open them in your favourite text editor and browse through their contents. It will quickly become apparent to you how to modify them or how to create new files for adding new devices to your existing TangoDB.

# Logs

Logs can be checked with `podman logs <container>`:

```shell
podman logs dsconfig
wait-for-it.sh: waiting 30 seconds for databaseds:10000
wait-for-it.sh: databaseds:10000 is available after 0 seconds
```



# Container restart

 The containers will automatically be restarted on reboot or failure. You can stop them explicitly to prevent them from automatically restarting:

```shell
make stop <container>
```

# When things go wrong

## `podman` related issues

There are plenty. `podman` is really not a nice container engine to work with. It is very fragile and not user friendly. I spent a lot of time trying to work around issue in `podman`, just to find out that there is no sensible  workaround possible. By the way, that is the reason why I have reintroduced `docker` and make it possible to choose between `docker` and `podman`. I am just so tired of fiddling with `podman`.

- The VM starts but the Makefile stops at:
    ```bash
    Starting machine "tango-playground"
    Waiting for VM ...
    ```

    Usually there is nothing wrong. This is one of `podman`'s inconsistencies. Just `CTRL-C` and run the same target again.

- The VM starts but the `Makefile` stops with an error:
    ```bash
    Starting machine "tango-playground"
    Waiting for VM ...
    Mounting volume... /Users/thomas:/Users/thomas
    Error: exit status 255
    make: *** [podman_start] Error 125
    ```

    Again, there is usually nothing wrong here. Just re-run the same target.

- `podman` VM does not work any more
    You'll see something like this:

    ```bash
    thomas@okeanos tango-playground (add_docker_as_alternative) 29: make status
    DISPLAY=192.168.222.136:0 CONTAINER_SCRATCH=/Users/thomas/workspace.thomas/tango/tango-playground/container-scratch/:/home/tango:rw XAUTHORITY=/hosthome/.Xauthority TANGO_HOST=databaseds:10000 MYSQL_HOST=tangodb:3306 HOSTHOME=/Users/thomas:/hosthome:rw HOSTNAME=okeanos.senmut.net LOG_HOSTNAME=localhost NETWORK_MODE=tangonet XAUTHORITY_MOUNT=/Users/thomas/.Xauthority:/hosthome/.Xauthority:ro CONTAINER_NAME_PREFIX= COMPOSE_IGNORE_ORPHANS=true CONTAINER_EXECUTION_UID=502 CONTAINER_EXECUTION_GID=20 DOCKER_HOST=unix:///Users/thomas/.local/share/containers/podman/machine/tango-playground/podman.sock docker-compose --env-file /Users/thomas/workspace.thomas/tango/tango-playground/containers/.env -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/archiver.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/hdbpp-configurator.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/itango.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/jive.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/networks.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/pogo.yml -f /Users/thomas/workspace.thomas/tango/tango-playground/containers/tango.yml ps
    Traceback (most recent call last):
      File "/Users/thomas/workspace.thomas/tango/tango-playground/env/lib/python3.9/site-packages/urllib3/connectionpool.py", line 703, in urlopen
        httplib_response = self._make_request(
      File "/Users/thomas/workspace.thomas/tango/tango-playground/env/lib/python3.9/site-packages/urllib3/connectionpool.py", line 398, in _make_request
        conn.request(method, url, **httplib_request_kw)
      File "/usr/local/Cellar/python@3.9/3.9.12_1/Frameworks/Python.framework/Versions/3.9/lib/python3.9/http/client.py", line 1285, in request
    ```

    This means that `podman` cannot reach its own VM any more. Yeah, I know. It is time to restart the `podman` VM by running the `podman_exit` and `podman_start` targets:
    ```bash
    make podman_exit
    make podman_start
    ```

## `docker` related issues

With `docker` I did not encounter any issues so far. It apparently just works.
